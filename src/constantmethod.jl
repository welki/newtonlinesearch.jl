@with_kw struct Constant{T<:Number} <: LineSearchMethod
  λ::T = 1.0
end

struct ConstantState{T<:Number} <: MethodState
  iteration::Integer
  λ::T
end

struct ConstantTrace{T<:Number} <: LineSearchTrace
  states::Vector{ConstantState{T}}
end

function Base.show(io::IO, state::ConstantState)
  @printf io "%8d   %11g\n" state.iteration state.λ
end

function (lsmethod::Constant)(f::Function, df::Function, x::T, s::T) where T
  return lsmethod.λ, ConstantTrace([ConstantState(0, lsmethod.λ)])
end
