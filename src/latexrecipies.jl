@latexrecipe function latex_rootmethodtrace(trace::RootMethodTrace)
  header = hcat(raw"{Iteration}", raw"{Estimate $x$ of zero}",
                raw"{$\norm{F(x)}_\infty$}", raw"{$\norm{\Delta x}_2$}",
                raw"{Step Size $\lambda$}")
  
  table = zeros(Number, length(trace.states), length(header))
  
  for (i, state) in enumerate(trace.states)
    table[i,:] = Number[state.iteration, state.x, state.fnorm, state.xnorm, state.λ]
  end

  # use Number as type, else Int iteration is promoted to Float64 by default
  # then siunitx adds unneccessary zeros after decimal point for iteration
  # should not have massive performance impact as table small
  # alternative: include siunitx column type options S support in latexify
  # and specify truncation for iteration column

  # replace NaN as siunitx cannot replace (it displays NaN)
  fmt --> x -> isnan(x) ? "{--}" : x
  env --> :tabular
  latex --> false
  booktabs --> true
  adjustment --> :S
  head --> header

  return table
end

@latexrecipe function latex_linesearchtrace(trace::LineSearchTrace)
  header = hcat(raw"{Iteration}", raw"{$\lambda$}")

   table = zeros(Number, length(trace.states), length(header))
  
  for (i, state) in enumerate(trace.states)
    table[i,:] = Number[state.iteration, state.λ]
  end

  fmt --> x -> isnan(x) ? "{--}" : x
  env --> :tabular
  latex --> false
  booktabs --> true
  adjustment --> :S
  head --> header

  return table
end
