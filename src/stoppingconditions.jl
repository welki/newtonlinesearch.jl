@with_kw struct MaxIterations <: IterativeStoppingCondition
  maxiter::Int = 1000
end

@with_kw struct Residual{T<:Real} <: IterativeStoppingCondition
  ftol::T = 1e-8
end

@with_kw struct StepNorm{T<:Real} <: IterativeStoppingCondition
  xtol::T = 1e-16
end

@with_kw struct CompositeCondition <: IterativeStoppingCondition
  conditions::Set{IterativeStoppingCondition} = Set([MaxIterations(),
                                                     Residual(), StepNorm()])
end

(condition::MaxIterations)(state::MethodState) = state.iteration >= condition.maxiter

(condition::Residual)(state::MethodState) = state.fnorm < condition.ftol

(condition::StepNorm)(state::MethodState) = state.xnorm < condition.xtol

function (composite::CompositeCondition)(state::MethodState)
  for c in composite.conditions
    if c(state)
      return true
    end
  end
  return false
  # any([state] .|> composite.conditions) -> much slower in benchmark (+2x)
  # little bit faster without compositecondition
end
