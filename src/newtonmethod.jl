struct NewtonState{T<:Number,S} <: MethodState
  iteration::Int
  x::S
  fnorm::T
  xnorm::T
  λ::T
end

struct NewtonTrace{T<:Number,R<:LineSearchTrace,S} <: RootMethodTrace
  states::Vector{NewtonState{T,S}}
  lstraces::Vector{R}

  #NewtonTrace{T<:Number}(state::NewtonState{T}) = new(state)
end

function Base.push!(trace::NewtonTrace, state::NewtonState,
                    lstrace::LineSearchTrace)
  push!(trace, state)
  push!(trace.lstraces, lstrace)
end

function Base.show(io::IO, state::NewtonState{T,S}) where {T<:Number,S<:Number}
  @printf(io, "%9d    %18g    %13g    %9g    %11g\n", state.iteration, state.x,
          state.fnorm, state.xnorm, state.λ)
end

# for x not a number (e.g. x = [1, 0])
function Base.show(io::IO, state::NewtonState)
   @printf(io, "%9d    %18s    %13g    %9g    %11g\n", state.iteration, repr(state.x),
           state.fnorm, state.xnorm, state.λ)
end

function newtonmethod(F::Function, DF::Function, x₀::T,
                      stoppingcondition::IterativeStoppingCondition=CompositeCondition();
                      lsmethod::LineSearchMethod=Constant(),
                      f::Function=x -> 0.5*dot(F(x),F(x)),
                      df::Function=x -> F(x)' * DF(x)) where T
  x = copy(x₀)
  trace = NewtonTrace([NewtonState(0, x, norm(F(x), Inf),
                                   convert(eltype(x), NaN),
                                   convert(eltype(x), NaN))],
                      Vector{LineSearchTrace}(undef,0))

  iteration = 0
  
  while !stoppingcondition(trace[end])
    s = -DF(x) \ F(x)
    λ, lstrace = lsmethod(f, df, x, s)
    Δx = λ*s
    x += Δx

    iteration += 1
    fnorm = norm(F(x), Inf)
    xnorm = norm(Δx, 2)
    push!(trace, NewtonState(iteration, x, fnorm, xnorm, λ), lstrace)
  end
  
  return trace
end
