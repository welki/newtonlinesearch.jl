@with_kw struct Armijo{T<:Number} <: LineSearchMethod
  λ₀::T = 1.0
  γ::T = 1e-4
  δ::T = 0.01
  β₁::T = 0.5
  β₂::T = 0.5
  β::T = 0.5
  useβ::Bool = true
  maxiterations::Int = 1000
end

struct ArmijoState{T<:Number} <: MethodState
  iteration::Int
  λ::T
end

struct ArmijoTrace{T<:Number} <: LineSearchTrace
  states::Vector{ArmijoState{T}}
end

function Base.show(io::IO, state::ArmijoState)
  @printf io "%8d   %11g\n" state.iteration state.λ
end

function (lsmethod::Armijo)(f::Function, df::Function, x::T, s::T) where T

  λ = lsmethod.λ₀

  trace = ArmijoTrace([ArmijoState(0, λ)])
  
  if stepsizesmall(λ, lsmethod.γ, df, x, s)
    throw(SmallStepSizeError(λ,
                             "step size to small. Try choosing a smaller γ."))
  end
  
  iteration = 0
  
  while !sufficientdescent(f, df, x, λ, lsmethod.δ, s)
    
    if iteration == lsmethod.maxiterations
      throw(MaxIterationsError(iteration,
                               "maximum number of iterations reached." *
                               "Try using a higher value for maxiterations."))
    end
    
    if lsmethod.useβ
      λ *= lsmethod.β
    end
    
    iteration += 1

    push!(trace, ArmijoState(iteration, λ))
  end
  
  return λ, trace
end

function stepsizesmall(λ::T, γ::T, df::Function, x::Union{T,S},
                       s::Union{T,S}) where {T<:Number, S<:AbstractArray{T}}
  return λ < -γ * df(x) * s / dot(s,s)
end

function sufficientdescent(f::Function, df::Function, x::T, λ::S, δ::S,
                           s::T) where {T,S}
  return f(x + λ*s) <= f(x) + δ*λ*df(x)*s
end
