struct SmallStepSizeError{T<:Number} <: Exception
  λ::T
  msg::String
end

struct MaxIterationsError <: Exception
  iterations::Int
  msg::String
end

Base.showerror(io::IO, e::SmallStepSizeError) = print(io, e.λ, e.msg)
Base.showerror(io::IO, e::MaxIterationsError) = print(io, e.iterations, e.msg)
