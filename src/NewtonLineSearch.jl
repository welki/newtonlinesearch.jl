__precompile__()
module NewtonLineSearch

using LinearAlgebra
using Parameters
using Printf
using Latexify

export newtonmethod
export Constant, Armijo
export MaxIterations, Residual, StepNorm, CompositeCondition


abstract type LineSearchMethod end
abstract type MethodState end
abstract type MethodTrace <: AbstractArray{MethodState, 1} end
abstract type RootMethodTrace <: MethodTrace end
abstract type LineSearchTrace <: MethodTrace end
abstract type IterativeStoppingCondition end

Base.size(trace::MethodTrace) = size(trace.states)
Base.IndexStyle(::Type{<:MethodTrace}) = IndexLinear()
Base.getindex(trace::MethodTrace, i::Int) = trace.states[i]
Base.push!(trace::MethodTrace, state::MethodState) = push!(trace.states, state)

function Base.show(io::IO, trace::RootMethodTrace)
  @printf(io, "Iteration    Estimate x of zero    F(x) inf-norm    Δx 2-norm    Step size λ\n")
  @printf(io, "---------    ------------------    -------------    ---------    -----------\n")
  for state in trace.states
    show(io, state)
  end
end

function Base.show(io::IO, trace::LineSearchTrace)
  @printf io "Iteration    Step size λ\n"
  @printf io "---------    -----------\n"
  for state in trace.states
    show(io, state)
  end
end

function Base.show(io::IO, traces::Vector{LineSearchTrace})
  for (i, trace) in enumerate(traces)
    @printf io "%d:\n" i
    show(io, trace)
    print("========================\n")
  end
end

function Base.show(io::IO, ::MIME"text/plain", trace::MethodTrace)
  @printf io "%d-element %s:\n" length(trace) typeof(trace)
  show(io, trace)
end

function Base.show(io::IO, ::MIME"text/plain", traces::Vector{LineSearchTrace})
  @printf io "%d-element %s:\n" length(traces) typeof(traces)
  show(io, traces)
end

#Base.push!(trace::MethodTrace, state::MethodState) = push!(trace.states, state)
#Base.getindex(trace::MethodTrace) = getindex(trace.)

# Newton method
include("newtonmethod.jl")

# Line search methods
include("armijomethod.jl")
include("constantmethod.jl")

# Stopping conditions
include("stoppingconditions.jl")

# Exception types
include("exceptiontypes.jl")

# Latexify recipes
include("latexrecipies.jl")

end # module
