# NewtonLineSearch.jl

This package implements Newton's method and a line search globalized Newton method
using the Armijo–Goldstein step size method. It is mainly written for demonstration
purposes.

## Installation

Open the Julia REPL and type `]` to enter the Pkg REPL. Then, add this package to
your current environment by
```julia
(@v1.5) pkg> add https://gitlab.com/welki/newtonlinesearch.jl
```

## Usage

The package exports the function
```julia
newtonmethod(F::Function, DF::Function, x₀::T,
             stoppingcondition::IterativeStoppingCondition=CompositeCondition();
             lsmethod::LineSearchMethod=Constant(),
             f::Function=x -> 0.5*dot(F(x), F(x)),
             df::Function=x -> F(x)' * DF(x)) where T
```
which solves $`F(x) = 0`$, where `DF` is the derivative of `F`. It returns a table of
the form
```
Iteration   Estimate x of zero  F(x) inf-norm   Δx 2-norm   Step size λ
---------   ------------------  -------------   ---------   -----------
```

There are three different stopping conditions:

* `MaxIterations(maxiter=1000)`
* `Residual(ftol=1e-8)`
* `StepNorm(xtol=1e-16)`

They can be combined with `CompositeCondition(Set([...]))`. For example,

```julia
stoppingcondition=CompositeCondition(Set[MaxIterations(500), Residua(1e-9)])
```

would make the method stop if 500 iterations are reached or the residual is
smaller than $`10^{-9}`$.

By default, `CompositeCondition()` uses all three above defined stopping conditions
with their default values.

# Examples

In order to use Newton's method for solving $`F(x) = 0`$, it is sufficient to write
```julia
newtonmethod(F, DF, x₀)
```

For the Armijo–Goldstein step size method, use instead
```julia
newtonmethod(F, DF, x₀, lsmethod=Armijo())
```

For further examples, please have a look at
[this repository](https://gitlab.com/welki/bachelor-thesis-code.git).
